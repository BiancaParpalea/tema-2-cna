﻿using Grpc.Net.Client;
using GrpcServer;
using System;
using System.Text.RegularExpressions;
using System.Threading.Tasks;

namespace GrpcClient
{
    class Program
    {
        private static bool IsLeapYear(int year)
        {
            if (((year % 4 == 0) && (year % 100 != 0)) || (year % 400 == 0))
                return true;
            else return false;
        }
        public static bool VerificationDate(string date)
        {
            //verificare daca e de tipul dd/mm/yyyy sau d/m/yyyy
            Regex regex = new Regex(@"^\d{1,2}/\d{1,2}/\d{4}$", RegexOptions.IgnorePatternWhitespace);
            
            if (regex.Match(date).Success == false)
                return false;

            //verificari pt zi si luni corecte + an bisect
            string[] words = date.Split('/');
            int day = Int32.Parse(words[0]);
            int month = Int32.Parse(words[1]);
            int year = Int32.Parse(words[2]);

            if (month < 1 && month > 12 || day < 1 || day > 31)
                return false;

            if (day > 30 && (month == 4 || month == 6 || month == 9 || month == 11))
                return false;

            if (day > 29 && month == 2 && IsLeapYear(year) == true)
                return false;
            
            return true;
        }

        static async Task Main(string[] args)
        {
            var channel = GrpcChannel.ForAddress("https://localhost:5001");
            var client = new Greeter.GreeterClient(channel);
            
            bool wasTransmitted = false;
            Console.WriteLine("Write the date(dd/mm/yyyy) : ");
            string date = Console.ReadLine();

            while (wasTransmitted == false)
            {
                if (VerificationDate(date) == true)
                {
                    wasTransmitted = true;
                    var reply = await client.ShowSignAsync(new SignRequest { Date = date }); //trimit catre server
                    Console.WriteLine(reply.Message);
                }
                else
                {
                    Console.WriteLine("\nYour date was incorect! Please write your date again: ");
                    date = Console.ReadLine();
                }
            }
            
            Console.ReadKey();
        }
    }
}
