using System;
using System.IO;
using System.Threading.Tasks;
using Grpc.Core;
using Microsoft.Extensions.Logging;

namespace GrpcServer
{
    public class GreeterService : Greeter.GreeterBase
    {
        private readonly ILogger<GreeterService> _logger;
        public GreeterService(ILogger<GreeterService> logger)
        {
            _logger = logger;
        }

        public override Task<SignReply> ShowSign(SignRequest request, ServerCallContext context)
        {
            Console.WriteLine("The server has started.\n" + context.Host);
            
            string[] givenDate = request.Date.ToString().Split('/');

            int givenDay = Int32.Parse(givenDate[0]);
            int givenMonth = Int32.Parse(givenDate[1]);

            string signName = string.Empty;
            string[] dates = File.ReadAllLines("horoscop.txt");

            for (int index = 0; index < dates.Length; index++)
            {
                string[] dateSplit = dates[index].Split(' ');
                signName = dateSplit[0];
                var signStartDate = DateTime.Parse(dateSplit[1]);
                var signFinishDate = DateTime.Parse(dateSplit[2]);

                if ((givenMonth == signStartDate.Month && givenDay >= signStartDate.Day) || (givenMonth == signFinishDate.Month && givenDay <= signFinishDate.Day))
                {
                    break;
                }
            }

            return Task.FromResult(new SignReply
            {
                Message = "\nYour sign is: " + signName + "."
            });
        }
    }
}
